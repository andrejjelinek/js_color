// import { SwiperSlide, Swiper } from 'swiper/react'
import { useGSAP } from '@gsap/react'
import { useRef } from 'react'
import { gsap } from 'gsap'
import { ScrollTrigger } from 'gsap/ScrollTrigger'

const Technology = () => {
  const container = useRef(null)
  gsap.registerPlugin(useGSAP)
  gsap.registerPlugin(ScrollTrigger)

  useGSAP(
    () => {
      const timeline = gsap.timeline({
        scrollTrigger: {
          trigger: '.title-technology',
          scrub: 3,
          start: 'clamp(20px bottom)',
          end: 'clamp(top 20%)',
          toggleActions: 'restart pause reverse pause',
        },
      })

      timeline
        .to('.title-technology ', {
          x: 0,
          opacity: 1,
        })
        .to('.title-1', {
          x: 0,
          opacity: 1,
        })
        .to('.list-1', {
          x: 0,
          opacity: 1,
        })
        .to('.image-1 ', {
          opacity: 1,
        })

      const timeline2 = gsap.timeline({
        scrollTrigger: {
          trigger: '.title-2',
          scrub: 3,
          start: 'clamp(20px bottom)',
          end: 'clamp(top 40%)',
          toggleActions: 'restart pause reverse pause',
        },
      })

      timeline2
        .to('.title-2', {
          x: 0,
          opacity: 1,
        })
        .to('.list-2', {
          x: 0,
          opacity: 1,
        })
        .to('.image-2 ', {
          opacity: 1,
        })
    },
    { scope: container }
  )

  return (
    <section id='technology' ref={container} className='mt-36 sm:w-5/6 sm:mx-auto '>
      <h2 className='title title-technology -translate-x-[25rem]'>Technológie</h2>

      {/* <div className='sm:hidden'>
        <Swiper spaceBetween={50} slidesPerView={1}>
          <SwiperSlide>
            <div>
              <div className='w-5/6 mx-auto'>
                <p className='text-2xl font-semibold text-dark-color'>Prášková lakovňa</p>
                <ul className='mx-5 list-disc'>
                  <li className='text-lg'>Trojdráhový podvesný dopravník s kolmou presuvnou rampou </li>
                  <li className='text-lg'>Plynová pec s dvoma horákmi s max výkonom 160kW</li>
                  <li className='text-lg'>Nanášacia kabína dľžky 6m, pre 2 osoby s vymrazovacou jednotkou vzduchu</li>
                  <li className='text-lg'>Maximálne rozmery lakovaných dielov sú: 6 x 1,8 x 1,5 (D x V x S [m])</li>
                  <li className='text-lg'>Maximálna hmotnosť lakovaných dielov u nás je: 1500 kg</li>
                </ul>
              </div>

              <figure className='mt-5 h-96'>
                <img className='object-cover w-full h-full' src='../../../public/images/technology1.jpg' alt='' />
              </figure>
            </div>
          </SwiperSlide>
          <SwiperSlide>
            <div>
              <div className='w-5/6 mx-auto'>
                <p className='text-2xl font-semibold text-dark-color'>Práškové lakovanie</p>

                <p className='text-lg'>Princíp páškového lakovania spočíva v nanesení práškovej farby na materiál a následnom vytvrdení práškovej farby vo vypaľovacej peci. Aplikačné metódy:</p>
                <ul className='mx-5 list-disc mt-2'>
                  <li className='text-lg'>
                    Elektrostatické nabíjanie (korona) Prášková farba je nabitá pomocou elektródy vysokého napätia na konci práškovej pištole. Elektróda vytvára pred práškovou pištoľou elektrické pole v ktorom sa prášková farba nabíja. Tento spôsob
                    nabíjania je veľmi rýchly a účinný, avšak nie je vhodný pre tvarovo zložité diely.
                  </li>
                  <li className='text-lg'>Elektrokinetické nabíjanie (tribo)</li>
                </ul>
              </div>

              <figure className='mt-5 h-96'>
                <img className='object-cover w-full h-full' src='../../../public/images/technology2.jpg' alt='' />
              </figure>
            </div>
          </SwiperSlide>
          ...
        </Swiper>
      </div>

      <div className='hidden sm:block shadow-lg test'>
        <div className='flex '>
          <div className='w-1/2 p-12 bg-dark-color text-light-color'>
            <p className='title-1 text-3xl font-semibold -translate-x-[20rem] opacity-0'>Prášková lakovňa</p>
            <ul className='list-1 mx-5 list-disc mt-6 -translate-x-[20rem] opacity-0'>
              <li className='text-lg mt-2 '>Trojdráhový podvesný dopravník s kolmou presuvnou rampou </li>
              <li className='text-lg mt-2 '>Plynová pec s dvoma horákmi s max výkonom 160kW</li>
              <li className='text-lg mt-2 '>Nanášacia kabína dľžky 6m, pre 2 osoby s vymrazovacou jednotkou vzduchu</li>
              <li className='text-lg mt-2 '>Maximálne rozmery lakovaných dielov sú: 6 x 1,8 x 1,5 (D x V x S [m])</li>
              <li className='text-lg mt-2 '>Maximálna hmotnosť lakovaných dielov u nás je: 1500 kg</li>
            </ul>
          </div>

          <figure className='image-1 w-1/2 sm:h-[30rem] opacity-0'>
            <img className='object-cover w-full h-full' src='../../../public/images/technology1.jpg' alt='' />
          </figure>
        </div>

        <div className='flex '>
          <figure className='image-2 w-1/2 sm:h-[30rem] opacity-0'>
            <img className='object-cover w-full h-full' src='../../../public/images/technology2.jpg' alt='' />
          </figure>

          <div className='w-1/2 p-12 bg-dark-color text-light-color'>
            <p className='title-2 text-3xl font-semibold -translate-x-[-20rem] opacity-0'>Práškové lakovanie</p>

            <p className='list-2 text-lg mt-6 -translate-x-[-20rem] opacity-0'>Princíp páškového lakovania spočíva v nanesení práškovej farby na materiál a následnom vytvrdení práškovej farby vo vypaľovacej peci. Aplikačné metódy:</p>

            <ul className='list-2 mx-5 list-disc mt-6 -translate-x-[-20rem] opacity-0'>
              <li className='text-lg mt-2'>
                Elektrostatické nabíjanie (korona) Prášková farba je nabitá pomocou elektródy vysokého napätia na konci práškovej pištole. Elektróda vytvára pred práškovou pištoľou elektrické pole v ktorom sa prášková farba nabíja. Tento spôsob
                nabíjania je veľmi rýchly a účinný, avšak nie je vhodný pre tvarovo zložité diely.
              </li>
              <li className='text-lg mt-2'>Elektrokinetické nabíjanie (tribo)</li>
            </ul>
          </div>
        </div>
      </div> */}
    </section>
  )
}

export default Technology
