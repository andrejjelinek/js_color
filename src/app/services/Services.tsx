import { useRef } from "react";
import { useGSAP } from "@gsap/react";
import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";
import { SprayCan, SquareCheck, Microwave, CalendarArrowUp, Cpu, ShieldCheck } from "lucide-react";

gsap.registerPlugin(ScrollTrigger);

const Services = () => {
  const containerRef = useRef<HTMLDivElement | null>(null);
  const titleRef = useRef<HTMLParagraphElement | null>(null);
  const subtitleRef = useRef<HTMLParagraphElement | null>(null);
  const cardsRef = useRef<(HTMLDivElement | null)[]>([]);

  useGSAP(() => {
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: containerRef.current,
        start: "top 80%", // Spustí sa, keď sekcia dosiahne 80% viewportu
      },
    });

    tl.from(titleRef.current, {
      y: -50,
      opacity: 0,
      duration: 1,
      ease: "power3.out",
    })
      .from(subtitleRef.current, {
        y: 20,
        opacity: 0,
        duration: 1,
        ease: "power3.out",
      }, "-=0.8") // Začne 0.8 sekundy pred koncom nadpisu
      .from(cardsRef.current, {
        opacity: 0,
        y: 30,
        stagger: 0.2, // Každá karta sa objaví postupne s oneskorením 0.2s
        duration: 1,
        ease: "power3.out",
      }, "-=0.5"); // Začne ešte pred koncom podnadpisu
  }, { scope: containerRef });

  const services = [
    { id: 1, title: "Kompletné riešenie", description: "Poskytujeme špičkové služby, ktoré zabezpečujú kvalitnú, estetickú a odolnú povrchovú úpravu kovových výrobkov.", icon: SquareCheck },
    { id: 2, title: "Práškové lakovanie", description: "Používame moderné technológie na striekanie práškovej farby na brány, ploty, okenné rámy a ďalšie kovové komponenty.", icon: SprayCan },
    { id: 3, title: "Vysokokapacitná pec", description: "S našou pecou s rozmermi až 10x5 metrov dokážeme spracovať aj veľkorozmerné výrobky, pričom zabezpečujeme rovnomerné a odolné vypálenie farby.", icon: Microwave },
    { id: 4, title: "Rýchle dodanie", description: "Garantujeme krátke dodacie lehoty a spoľahlivé doručenie tovaru na miesto určenia.", icon: CalendarArrowUp },
    { id: 5, title: "Špičková technológia", description: "Naša lakovňa je vybavená modernými zariadeniami, ktoré spĺňajú najvyššie štandardy kvality a efektivity.", icon: Cpu },
    { id: 6, title: "Spoľahlivosť, kvalita a skúsenosti", description: "Vďaka moderným technológiám a skúsenostiam prinášame riešenia na mieru každému zákazníkovi.", icon: ShieldCheck },
  ];

  return (
    <section id="services" className="bg-secondary-color section-container" ref={containerRef}>
      <p ref={titleRef} className="section-title text-center">
        Naše služby
      </p>
      <p ref={subtitleRef} className="text-center font-semibold text-[34px] sm:text-[40px]">
        Lakujeme s precíznosťou a zárukou kvality
      </p>

      <div className="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-4 mt-12 justify-center">
        {services.map((service, index) => (
          <div
            key={service.id}
            ref={(el) => (cardsRef.current[index] = el)} // Nastavenie typu
            className="bg-white px-9 py-12 flex flex-col justify-center items-center mx-auto"
          >
            <div className="text-primary-color">
              <div className="bg-blue-100 rounded-full w-14 h-14 flex justify-center items-center">
                <service.icon size={32} />
              </div>
            </div>
            <p className="font-bold text-center mt-6">{service.title}</p>
            <p className="text-gray mt-2 text-center">{service.description}</p>
          </div>
        ))}
      </div>
    </section>
  );
};

export default Services;
