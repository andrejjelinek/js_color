import { useRef } from "react";
import { useGSAP } from "@gsap/react";
import gsap from "gsap";
import ScrollTrigger from "gsap/ScrollTrigger";
import { BadgeCheck, Clock3 } from "lucide-react";

gsap.registerPlugin(ScrollTrigger);

const AboutSection = () => {
  const containerRef = useRef(null);
  const titleRef = useRef(null);
  const imageRef = useRef(null);
  const textRef = useRef(null);

  useGSAP(() => {
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: containerRef.current,
        start: "top 80%", // Animácia sa spustí, keď sekcia dosiahne 80% viewportu
      },
    });

    tl.from(titleRef.current, {
      x: -300,
      opacity: 0,
      duration: 1.2,
      ease: "power3.out",
    })
      .from(
        imageRef.current,
        {
          scale: 0.8,
          opacity: 0,
          duration: 1.2,
          ease: "power3.out",
        },
        "-=0.8" // Začne 0.8 sekundy pred koncom predchádzajúcej animácie
      )
      .from(
        textRef.current,
        {
          y: 50,
          opacity: 0,
          duration: 1,
          ease: "power3.out",
        },
        "-=0.6"
      );
  }, { scope: containerRef });

  return (
    <section id="about" className="section-container" ref={containerRef}>
      <p ref={titleRef} className="section-title">
        O nás
      </p>
      <div className="grid grid-cols-1 lg:grid-cols-2 gap-16 mt-7">
        <div ref={textRef}>
          <p className="font-bold text-3xl mb-12">
            Sme odborníci na kvalitnú povrchovú úpravu kovov
          </p>

          <p className="text-gray leading-[25px]">
            V našej práškovej lakovni sa špecializujeme na kvalitnú povrchovú úpravu kovových výrobkov.
          </p>

          <div className="mt-7">
            <div className="flex gap-4">
              <div className="text-primary-color">
                <div className="bg-blue-100 rounded-full w-14 h-14 flex justify-center items-center">
                  <BadgeCheck size={32} />
                </div>
              </div>
              <div>
                <p className="font-bold">Precízna povrchová úprava</p>
                <p className="text-gray mt-2">
                  Špecializujeme sa na striekanie práškovej farby na brány, ploty, okenné rámy a iné kovové komponenty. Farbu následne vypaľujeme v peci, čím dosahujeme jedinečnú odolnosť a vysokú kvalitu povrchu.
                </p>
              </div>
            </div>

            <div className="flex gap-4 mt-7">
              <div className="text-primary-color">
                <div className="bg-blue-100 rounded-full w-14 h-14 flex justify-center items-center">
                  <Clock3 size={32} />
                </div>
              </div>
              <div>
                <p className="font-bold">Dlhodobá odolnosť</p>
                <p className="text-gray mt-2">
                  Prášková farba poskytuje nielen atraktívny vzhľad, ale aj ochranu proti poveternostným vplyvom. Naším cieľom je dodať výrobky, ktoré vydržia roky a budú skvele vyzerať.
                </p>
              </div>
            </div>
          </div>
        </div>

        <div
          ref={imageRef}
          className="h-full w-full max-w-[500px] justify-self-center 2xl:justify-self-start"
        >
          <img
            className="rounded-[20px] w-full h-full object-cover"
            src="/aboutUs/about_us.jpg"
            alt="Striekanie image"
          />
        </div>
      </div>
    </section>
  );
};

export default AboutSection;
