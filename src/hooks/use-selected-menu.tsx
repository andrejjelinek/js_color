import { useEffect, useState } from 'react'

export const useSelectedMenu = () => {
	const [selectedSection, setSelectedSection] = useState<string>('introduction')
	const [bgColor, setBgColor] = useState('bg-transparent')

	const sections: string[] = ['introduction', 'about', 'services', 'gallery', 'contact']

	const scrollHandler = () => {
		sections.forEach((sectionId) => {
			const section = document.getElementById(sectionId)
			if (section) {
				const sectionTop = section.offsetTop
				const sectionHeight = section.scrollHeight
				const scrollPosition = window.scrollY + window.innerHeight / 2

				if (scrollPosition >= sectionTop && scrollPosition < sectionTop + sectionHeight) {
					setSelectedSection(sectionId)

					const aboutSection = document.getElementById('about')
					if (aboutSection && window.scrollY < aboutSection.scrollHeight) {
						if (bgColor !== 'bg-transparent') setBgColor('bg-transparent')
					} else {
						if (bgColor !== 'bg-[var(--menu-color)]') setBgColor('bg-[var(--menu-color)]')
					}
				}
			}
		})
	}

	useEffect(() => {
		window.addEventListener('scroll', scrollHandler)
		return () => {
			window.removeEventListener('scroll', scrollHandler)
		}
	}, [])

	return { selectedSection, setSelectedSection, bgColor }
}
