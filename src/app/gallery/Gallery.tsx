import { Carousel, CarouselContent, CarouselItem, CarouselNext, CarouselPrevious } from "@/components/ui/carousel";
import { photosSource } from "./photos";
import { gsap } from "gsap";
import { ScrollTrigger } from "gsap/ScrollTrigger";
import { useEffect, useRef, useState } from "react";

const Gallery = () => {
  const containerRef = useRef<HTMLDivElement>(null);
  const [isMobile, setIsMobile] = useState(window.innerWidth <= 640);

  gsap.registerPlugin(ScrollTrigger);

  useEffect(() => {
    const handleResize = () => {
      setIsMobile(window.innerWidth <= 640);
    };

    window.addEventListener("resize", handleResize);
    return () => window.removeEventListener("resize", handleResize);
  }, []);

  useEffect(() => {
    if (!containerRef.current) return;

    const images = containerRef.current.querySelectorAll(".gallery-img");

    gsap.fromTo(
      images,
      { autoAlpha: 0, y: 40 },
      {
        autoAlpha: 1,
        y: 0,
        duration: 1,
        stagger: 0.2,
        ease: "power3.out",
        scrollTrigger: {
          trigger: containerRef.current,
          start: "top 80%",
        },
      }
    );
  }, [isMobile]);

  // ✅ Rozdelenie `photosSource` na skupiny po 3 obrázkoch
  const groupedPhotos = [];
  for (let i = 0; i < photosSource.length; i += 3) {
    groupedPhotos.push(photosSource.slice(i, i + 3));
  }

  return (
    <section id="gallery" className="section-container">
      <p className="section-title text-center">Galéria</p>
      <p className="text-center font-semibold text-[34px] sm:text-[40px] mb-12">Ukážka našej práce</p>

      {isMobile ? (
        <Carousel className="w-full">
          <CarouselContent>
            {photosSource.map((photo, index) => (
              <CarouselItem key={index}>
                <div className="h-[40rem] w-full">
                  <figure className="overflow-hidden rounded-[20px]">
                    <img className="gallery-img object-cover w-full h-full scale-image" src={photo} alt="" />
                  </figure>
                </div>
              </CarouselItem>
            ))}
          </CarouselContent>
        </Carousel>
      ) : (
        <Carousel className="w-full">
          <CarouselContent ref={containerRef}>
            {groupedPhotos.map((photoGroup, index) => (
              <CarouselItem key={index}>
                <div className="grid grid-rows-2 grid-cols-12 gap-10 sm:gap-3 h-[40rem]">
                  {photoGroup.map((photo, i) => (
                    <figure
                      key={i}
                      className={`gallery-img ${
                        i === 0 ? "col-span-12 sm:col-span-6 row-span-1 sm:row-span-2" : "col-span-6 row-span-1"
                      } overflow-hidden rounded-[20px]`}
                    >
                      <img className="object-cover w-full h-full scale-image" src={photo} alt="" />
                    </figure>
                  ))}
                </div>
              </CarouselItem>
            ))}
          </CarouselContent>
          <CarouselPrevious />
          <CarouselNext />
        </Carousel>
      )}
    </section>
  );
};

export default Gallery;
