import { useRef } from 'react'
import { useGSAP } from '@gsap/react'
import gsap from 'gsap'
import { Facebook, Instagram, Mail, MapPin, Phone } from 'lucide-react'

const Contact = () => {
	const sectionRef = useRef<HTMLDivElement | null>(null)
	const titleRef = useRef<HTMLParagraphElement | null>(null)
	const subtitleRef = useRef<HTMLParagraphElement | null>(null)
	const contactCardRef = useRef<HTMLDivElement | null>(null)
	const mapRef = useRef<HTMLIFrameElement | null>(null)

	useGSAP(
		() => {
			const tl = gsap.timeline({
				scrollTrigger: {
					trigger: sectionRef.current,
					start: 'top 80%' // Animácia sa spustí, keď sekcia dosiahne 80% viewportu
				}
			})

			tl.from(titleRef.current, {
				opacity: 0,
				y: -30,
				duration: 0.8,
				ease: 'power3.out'
			})
				.from(
					subtitleRef.current,
					{
						opacity: 0,
						y: -20,
						duration: 0.8,
						ease: 'power3.out'
					},
					'-=0.5'
				)
				.from(
					contactCardRef.current,
					{
						opacity: 0,
						y: 30,
						duration: 1,
						ease: 'power3.out'
					},
					'-=0.5'
				)
				.from(
					mapRef.current,
					{
						opacity: 0,
						scale: 0.9,
						duration: 1,
						ease: 'power3.out'
					},
					'-=0.5'
				)
		},
		{ scope: sectionRef }
	)

	return (
		<section id='contact' ref={sectionRef} className='bg-secondary-color section-container'>
			<p ref={titleRef} className='section-title text-center'>
				Kontakt
			</p>
			<p ref={subtitleRef} className='text-center font-semibold text-[34px] sm:text-[40px]'>
				Neváhajte nás kontaktovať pre cenovú ponuku
			</p>

			<div className='flex flex-col mt-10 sm:flex-row justify-center gap-10'>
				<iframe ref={mapRef} className='h-[15rem] w-full sm:w-1/2 sm:h-[25rem]' src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d973.2771299745058!2d19.618940772611282!3d49.3646725674921!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4715c0d7289e666d%3A0x3ed04fa31c6a3a32!2zT3Jhdmlja8OhIDYzMy8zMSwgMDI4IDAxIFRyc3RlbsOh!5e0!3m2!1ssk!2ssk!4v1710784600407!5m2!1ssk!2ssk' loading='lazy'></iframe>

				<div ref={contactCardRef} className='flex flex-col mx-auto sm:mx-0 gap-5 bg-white p-10 w-full sm:w-1/2'>
					<p className='text-xl font-medium ml-9'>Jozef Šrámek</p>

					<div className='flex flex-col gap-4'>
						<div className='flex gap-3 items-center'>
							<Phone className='text-primary-color' size={24} />
							<a className='text-lg hover:underline' href='tel:+421 905 123 123'>
								+421 948 434 010
							</a>
						</div>

						<div className='flex gap-3 items-center'>
							<Mail className='text-primary-color' size={24} />
							<a className='text-lg hover:underline' href='mailto:jscolor23@gmail.com'>
								jscolor23@gmail.com
							</a>
						</div>

						<div className='flex gap-3 items-center'>
							<Instagram className='text-primary-color' size={24} />
							<a className='text-lg hover:underline' href='https://www.instagram.com/jscolor_ts?utm_source=ig_web_button_share_sheet&igsh=ZDNlZDc0MzIxNw=='>
								JS Color
							</a>
						</div>

						<div className='flex gap-3 items-center'>
							<Facebook className='text-primary-color' size={24} />
							<a className='text-lg hover:underline' href='https://www.instagram.com/jscolor_ts?utm_source=ig_web_button_share_sheet&igsh=ZDNlZDc0MzIxNw=='>
								JS Color
							</a>
						</div>

						<div className='flex items-center gap-3'>
							<MapPin className='text-primary-color text-2xl' size={24} />
							<p className='text-lg'>Oravická 633, Trstená, 028 01 Trstená</p>
						</div>
					</div>
				</div>
			</div>
		</section>
	)
}

export default Contact
