import About from './app/about/About'
import Contact from './app/contact/Contact'
import Gallery from './app/gallery/Gallery'
import Introduction from './app/introduction/Introduction'
import Services from './app/services/Services'

function App() {
	return (
		<>
			<Introduction />
			<About />
			<Services />
			<Gallery />
			<Contact />
			<div className='bg-[#000000] w-full py-2'>
				<p className='text-center text-white text-xs py-3'>© {new Date().getFullYear()} JS Color. Všetky práva vyhradené.</p>
			</div>
		</>
	)
}

export default App
