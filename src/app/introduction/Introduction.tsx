import Menu from './components/Menu'
import { useEffect, useRef } from 'react'
import { gsap } from 'gsap'
import { TextPlugin } from 'gsap/TextPlugin'

const Introduction = () => {
	const titleRef = useRef<HTMLParagraphElement>(null)
	const mainTitleRef = useRef<HTMLParagraphElement>(null)
	const descriptionRef = useRef<HTMLParagraphElement>(null)
	const changingTextRef = useRef<HTMLSpanElement>(null)

	gsap.registerPlugin(TextPlugin)

	useEffect(() => {
		const tl = gsap.timeline()

		// Fade-up animácia pre statické texty
		tl.fromTo(titleRef.current, { autoAlpha: 0, y: 40 }, { autoAlpha: 1, y: 0, duration: 1, ease: 'power3.out' }).fromTo(mainTitleRef.current, { autoAlpha: 0, y: 40 }, { autoAlpha: 1, y: 0, duration: 1.5, ease: 'power3.out' }, '-=0.5').fromTo(descriptionRef.current, { autoAlpha: 0, y: 40 }, { autoAlpha: 1, y: 0, duration: 1, ease: 'power3.out' }, '-=1')

		// Dynamická animácia pre meniaci sa text s fade-up efektom
		const changingTexts = ['okenných rámov', 'plotov', 'brán']
		let currentIndex = 0

		const changeText = () => {
			if (!changingTextRef.current) return // Null check

			gsap.fromTo(
				changingTextRef.current,
				{ autoAlpha: 0, y: 30 },
				{
					autoAlpha: 1,
					y: 0,
					duration: 1,
					ease: 'power3.out',
					onComplete: () => {
						setTimeout(() => {
							if (!changingTextRef.current) return // Null check

							gsap.to(changingTextRef.current, {
								autoAlpha: 0,
								y: -20,
								duration: 1,
								ease: 'power3.out',
								onComplete: () => {
									if (!changingTextRef.current) return // Null check

									currentIndex = (currentIndex + 1) % changingTexts.length
									changingTextRef.current.innerText = changingTexts[currentIndex]
									changeText()
								}
							})
						}, 2000) // Pauza pred zmenou textu
					}
				}
			)
		}

		if (changingTextRef.current) {
			changingTextRef.current.innerText = changingTexts[currentIndex]
			changeText()
		}
	}, [])

	return (
		<section id='introduction' className='h-screen flex flex-col bg-cover bg-[url(/images/introduction.png)] relative'>
			<div className='absolute inset-0 bg-black bg-opacity-50'></div>
			<Menu />

			<div className='flex flex-col justify-center align-middle h-full mx-auto items-center relative z-10 px-5'>
				<p ref={titleRef} className='font-semibold text-light-color text-[35px] opacity-0'>
					JS COLOR
				</p>

				<p ref={mainTitleRef} className='bg-gradient-to-r from-white to-[#b6ac62] bg-clip-text text-transparent text-center text-[40px] sm:text-[58px] font-bold opacity-0'>
					Prášková lakovňa Trstená
				</p>

				{/* Fixný text + meniaci sa text s pevným miestom */}
				<p ref={descriptionRef} className='text-light-color text-[24px] text-center opacity-0 mt-3'>
					Práškové lakovanie <span ref={changingTextRef} className='opacity-0 inline-block min-w-[280px] bg-gradient-to-r from-white to-[#b6ac62] bg-clip-text text-center font-bold text-[35px]'></span>
				</p>
			</div>
		</section>
	)
}

export default Introduction
