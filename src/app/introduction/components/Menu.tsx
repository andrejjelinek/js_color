import { useSelectedMenu } from '@/hooks/use-selected-menu'
import { useState } from 'react'

const Menu = () => {
	const [isMenuOpen, setIsMenuOpen] = useState(false)
	const { selectedSection, setSelectedSection, bgColor } = useSelectedMenu()

	const menuItems = [
		{ name: 'Úvod', id: 'introduction' },
		{ name: 'O nás', id: 'about' },
		{ name: 'Naše služby', id: 'services' },
		{ name: 'Galéria', id: 'gallery' },
		{ name: 'Kontakt', id: 'contact' }
	]

	const handleMenuItemClick = (id: string) => {
		setSelectedSection(id)
		setIsMenuOpen(false) // Close menu on mobile after item click
		const section = document.getElementById(id)
		if (section) {
			section.scrollIntoView({ behavior: 'smooth' })
		}
	}

	return (
		<nav className={`fixed p-4 md:p-0 top-0 left-0 w-full z-50 backdrop-blur-xl ${isMenuOpen && 'h-[100vh]'} ${bgColor}`}>
			<div className={`container mx-auto h-full flex md:items-center relative ${isMenuOpen ? 'flex-col' : 'flex-row justify-between '}`}>
				{!isMenuOpen ? (
					<div className='w-20 md:w-28'>
						<img className='h-full w-full object-cover' src='/images/logo.png' alt='logo' />
					</div>
				) : null}

				<button className='md:hidden' onClick={() => setIsMenuOpen(!isMenuOpen)}>
					{isMenuOpen ? (
						<div className='absolute right-0 top-0 p-4'>
							<svg xmlns='http://www.w3.org/2000/svg' className='h-8 w-8 text-primary-color' viewBox='0 0 20 20' fill='currentColor'>
								<path fillRule='evenodd' d='M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z' clipRule='evenodd' />
							</svg>
						</div>
					) : (
						<svg xmlns='http://www.w3.org/2000/svg' className='h-8 w-8 text-primary-color' fill='none' viewBox='0 0 24 24' stroke='currentColor'>
							<path strokeLinecap='round' strokeLinejoin='round' strokeWidth={2} d='M4 6h16M4 12h16M4 18h16' />
						</svg>
					)}
				</button>

				<ul className={`self-center mt-14 md:mt-0  md:flex md:items-center md:gap-6 transition-all duration-300 ${isMenuOpen ? 'block' : 'hidden'} md:block`}>
					{menuItems.map((item) => (
						<li key={item.id} className='md:my-0 my-4 text-center md:text-start'>
							{' '}
							{/* Added margin for mobile */}
							<a
								href={`#${item.id}`} // Use anchor links for in-page navigation
								className={`text-2xl md:text-lg hover:text-primary-color ${selectedSection === item.id ? 'text-primary-color' : 'text-white'}`}
								onClick={() => handleMenuItemClick(item.id)}>
								{item.name}
							</a>
						</li>
					))}
				</ul>
			</div>
		</nav>
	)
}

export default Menu
